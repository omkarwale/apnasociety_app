package com.ty.apnasociety.ApnaSociety_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.apnasociety.ApnaSociety_app.dto.Society;

public interface SocietyRepository extends JpaRepository<Society, Integer> {

}
