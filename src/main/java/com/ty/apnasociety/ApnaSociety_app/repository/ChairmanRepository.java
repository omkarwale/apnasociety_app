package com.ty.apnasociety.ApnaSociety_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.apnasociety.ApnaSociety_app.dto.Chairman;

public interface ChairmanRepository extends JpaRepository<Chairman, Integer> {

}
