package com.ty.apnasociety.ApnaSociety_app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.apnasociety.ApnaSociety_app.dto.Customer;
import com.ty.apnasociety.ApnaSociety_app.repository.CustomerRepsitory;

@Repository
public class CustomerDao {

	@Autowired
	private CustomerRepsitory customerRepsitory;

	public Customer saveCustomer(Customer customer) {
		return customerRepsitory.save(customer);
	}

	public List<Customer> getAllCustomers() {
		return customerRepsitory.findAll();
	}

	public void deleteCustomer(int id) {
		customerRepsitory.deleteById(id);
	}

	public Customer getCustomerById(int id) {
		Optional<Customer> optional = customerRepsitory.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public Customer updateCustomer(int id, Customer customer) {
		Customer customer2 = getCustomerById(id);
		if (customer2 != null) {
			customerRepsitory.save(customer);
			return customer;
		}
		return null;
	}
}
