package com.ty.apnasociety.ApnaSociety_app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.apnasociety.ApnaSociety_app.dto.Society;
import com.ty.apnasociety.ApnaSociety_app.repository.SocietyRepository;

@Repository
public class SocietyDao {

	@Autowired
	private SocietyRepository societyRepository;
	
	public Society saveSociety(Society society) {
		return societyRepository.save(society);
	}
	
	public Society getSocietyById(int id) {
		Optional<Society> optional = societyRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public List<Society> getAllSocieties() {
		return societyRepository.findAll();
	}

	public Society updateSociety(int id, Society society) {
		Society society2 = getSocietyById(id);
		if (society2 != null) {
			societyRepository.save(society);
			return society;
		}
		return null;
	}

	public void deleteSociety(int id) {
		societyRepository.deleteById(id);
	}
}
