package com.ty.apnasociety.ApnaSociety_app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.apnasociety.ApnaSociety_app.dto.Complaints;
import com.ty.apnasociety.ApnaSociety_app.repository.ComplaintsRepository;

@Repository
public class ComplaintsDao {

	@Autowired
	ComplaintsRepository repository;

	public Complaints saveComplaints(int customerId, Complaints complaints) {
		return repository.save(complaints);
		
	}

	public Complaints getByIdComplains(int id) {
		Optional<Complaints> opt = repository.findById(id);
		if (opt.isEmpty()) {
			return null;
		} else {
			return opt.get();
		}
	}

	public Complaints UpdateComplaints(int id, Complaints complaints) {
		Complaints complaintsid = getByIdComplains(id);
		if (complaintsid != null) {
			return complaints;
		}
		return null;
	}

	public boolean deleteComplaints(int id) {
		Complaints complaints = getByIdComplains(id);
		if(complaints != null) {
			repository.delete(complaints);
			return true;
		}
		return false;
	}

	public List<Complaints> getAllComplaints() {
		return repository.findAll();
	}
}
