package com.ty.apnasociety.ApnaSociety_app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.apnasociety.ApnaSociety_app.dto.Chairman;
import com.ty.apnasociety.ApnaSociety_app.repository.ChairmanRepository;

@Repository
public class ChairmanDao {

	@Autowired
	private ChairmanRepository chairmanRepository;

	public Chairman saveChairman(Chairman chairman) {
		return chairmanRepository.save(chairman);
	}

	public Chairman getChairmanById(int id) {
		Optional<Chairman> optional = chairmanRepository.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		return optional.get();
	}

	public List<Chairman> getAllChairmans() {
		return chairmanRepository.findAll();
	}

	public Chairman updateChairman(int id, Chairman chairman) {
		Chairman chairman2 = getChairmanById(id);
		if (chairman2 != null) {
			chairmanRepository.save(chairman);
			return chairman;
		}
		return null;
	}

	public void deleteChairman(int id) {
		chairmanRepository.deleteById(id);
	}
}
