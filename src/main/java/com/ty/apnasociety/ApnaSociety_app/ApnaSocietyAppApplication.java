package com.ty.apnasociety.ApnaSociety_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApnaSocietyAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApnaSocietyAppApplication.class, args);
	}

}
