package com.ty.apnasociety.ApnaSociety_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.apnasociety.ApnaSociety_app.dto.ResponseStruture;
import com.ty.apnasociety.ApnaSociety_app.dto.Society;
import com.ty.apnasociety.ApnaSociety_app.service.SocietyService;

@RestController
public class SocietyController {

	@Autowired
	private SocietyService societyService;

	@PostMapping("chairman/{chairmanId}/society")
	public ResponseStruture<Society> saveSociety(@PathVariable int ChairmanId, @RequestBody Society society) {
		return societyService.saveSociety(ChairmanId, society);
	}

	@GetMapping("society")
	public ResponseStruture<Society>  getSocietyById(@RequestParam int id) {
		return societyService.getSocietyById(id);
	}

	@GetMapping("allsociety")
	public List<Society> getAllSocieties() {
		return societyService.getAllSocieties();
	}

	@PutMapping("society")
	public ResponseStruture<Society> updateSociety(@RequestParam int id, @RequestBody Society society) {
		return societyService.updateSociety(id, society);
	}

	@DeleteMapping("society")
	public ResponseStruture<Society> deleteSociety(@RequestParam int id) {
		return societyService.getSocietyById(id);
	}
}
