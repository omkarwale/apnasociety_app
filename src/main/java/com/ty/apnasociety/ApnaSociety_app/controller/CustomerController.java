package com.ty.apnasociety.ApnaSociety_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.apnasociety.ApnaSociety_app.dto.Customer;
import com.ty.apnasociety.ApnaSociety_app.dto.ResponseStruture;
import com.ty.apnasociety.ApnaSociety_app.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@PostMapping("society/{societyId}/customer")
	public ResponseStruture<Customer> saveCustomer(@PathVariable int societyId, @RequestBody Customer customer) {
		return customerService.saveCustomer(societyId, customer);
	}

	@GetMapping("customer")
	public ResponseStruture<Customer> getCustomerById(@RequestParam int id) {
		return customerService.getCustomerById(id);
	}

	@GetMapping("allcustomer")
	public List<Customer> getAllCustomers() {
		return customerService.getAllCustomers();
		
	}

	@PutMapping("customer")
	public ResponseStruture<Customer> updateCustomer(@RequestParam int id, @RequestBody Customer customer) {
		return customerService.updateCustomer(id, customer);
	}

	@DeleteMapping("customer")
	public ResponseStruture<Customer> deleteCustomer(@RequestParam int id) {
		return customerService.getCustomerById(id);
	}
}
