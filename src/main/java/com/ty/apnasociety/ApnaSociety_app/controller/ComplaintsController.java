package com.ty.apnasociety.ApnaSociety_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.apnasociety.ApnaSociety_app.dto.Complaints;
import com.ty.apnasociety.ApnaSociety_app.dto.ResponseStruture;
import com.ty.apnasociety.ApnaSociety_app.service.ComplaintsService;

@RestController
public class ComplaintsController {
	@Autowired
	private ComplaintsService complaintsService;
	@PostMapping("/complaints")
	public ResponseStruture<Complaints> saveComplaints(@RequestParam int customer_id   ,@RequestBody Complaints complaints) {
		return complaintsService.saveComplaints(customer_id, complaints);
	}

	@PutMapping("/complaints")
	public ResponseStruture<Complaints> updateComplaints(@RequestParam int customer_id   ,@RequestBody Complaints complaints) {
		return complaintsService.updateComplaints(customer_id, complaints);
	}
	@GetMapping("/complaints")
	public ResponseStruture<Complaints> getComplaintsById(@RequestParam int complaints_id) {
		return complaintsService.getComplaintsById(complaints_id);
	}
	@GetMapping("/allcomplaints")
	public List<Complaints> getAllComplaints(){
		return getAllComplaints();
	}
	@DeleteMapping("/complaints")
	public ResponseStruture<Complaints> deleteComplaint(@RequestParam int complaints_id) {
		return complaintsService.deleteComplaint(complaints_id);
	}

}
