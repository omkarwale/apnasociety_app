package com.ty.apnasociety.ApnaSociety_app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.apnasociety.ApnaSociety_app.dto.Chairman;
import com.ty.apnasociety.ApnaSociety_app.dto.ResponseStruture;
import com.ty.apnasociety.ApnaSociety_app.service.ChairmanService;

@RestController
public class ChairmanController {

	@Autowired
	private ChairmanService chairmanService;
	
	@PostMapping("/chairman")
	public ResponseStruture<Chairman> saveChairman(@RequestBody Chairman chairman) {
		return chairmanService.saveChairman(chairman);
	}
	
	@GetMapping("/chairman")
	public ResponseStruture<Chairman> getChairmanById(@RequestParam int id) {
		return chairmanService.getChairmanById(id);
	}
	
	@GetMapping("/allchairman")
	public List<Chairman> getAllChairmans(){
		return chairmanService.getAllChairmans();
	}
	
	@PutMapping("/chairman")
	public ResponseStruture<Chairman> updateChairman(@RequestParam int id,@RequestBody Chairman chairman ) {
		return chairmanService.updateChairman(id, chairman);
	}
	
	@DeleteMapping("/chairman")
	public void deleteChairman(@RequestParam int id) {
		chairmanService.deleteChairman(id);
	}
}
