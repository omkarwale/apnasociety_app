package com.ty.apnasociety.ApnaSociety_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.apnasociety.ApnaSociety_app.dao.ChairmanDao;
import com.ty.apnasociety.ApnaSociety_app.dao.SocietyDao;
import com.ty.apnasociety.ApnaSociety_app.dto.Chairman;
import com.ty.apnasociety.ApnaSociety_app.dto.ResponseStruture;
import com.ty.apnasociety.ApnaSociety_app.dto.Society;
import com.ty.apnasociety.ApnaSociety_app.exception.NoIdFoundException;

@Service
public class SocietyService {

	@Autowired
	private SocietyDao societyDao;

	@Autowired
	private ChairmanDao chairmanDao;

	public ResponseStruture<Society> saveSociety(int chairmanId, Society society) {
		Chairman chairman = chairmanDao.getChairmanById(chairmanId);
		if (chairman == null) {
			throw new NoIdFoundException();
		}
		ResponseStruture<Society> struture = new ResponseStruture<Society>();
		struture.setData(society);
		struture.setMsg("Socity is saved");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public ResponseStruture<Society> getSocietyById(int id) {
		Society society = societyDao.getSocietyById(id);
		if (society == null) {
			throw new NoIdFoundException();
		}
		ResponseStruture<Society> struture = new ResponseStruture<Society>();
		struture.setData(society);
		struture.setMsg("Socity is Available");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public List<Society> getAllSocieties() {
		List<Society> society = societyDao.getAllSocieties();
		if (society == null) {
			throw new NoIdFoundException();
		}
		return society;
	}

	public ResponseStruture<Society> updateSociety(int id, Society society) {
		Society society2 = societyDao.getSocietyById(id);
		if (society2 == null) {
			throw new NoIdFoundException();
		}
		ResponseStruture<Society> struture = new ResponseStruture<Society>();
		struture.setData(society);
		struture.setMsg("Socity is updated");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public ResponseStruture<Society> deleteSociety(int id) {
		Society society = societyDao.getSocietyById(id);
		if (society == null) {
			throw new NoIdFoundException();
		}
		societyDao.deleteSociety(id);
		ResponseStruture<Society> struture = new ResponseStruture<Society>();
		struture.setData(society);
		struture.setMsg("Socity is deleted");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}
}
