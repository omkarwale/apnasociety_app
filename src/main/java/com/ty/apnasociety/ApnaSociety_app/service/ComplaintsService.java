package com.ty.apnasociety.ApnaSociety_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.apnasociety.ApnaSociety_app.dao.ComplaintsDao;
import com.ty.apnasociety.ApnaSociety_app.dao.CustomerDao;
import com.ty.apnasociety.ApnaSociety_app.dto.Complaints;
import com.ty.apnasociety.ApnaSociety_app.dto.Customer;
import com.ty.apnasociety.ApnaSociety_app.dto.ResponseStruture;
import com.ty.apnasociety.ApnaSociety_app.exception.NoIdFoundException;

@Service
public class ComplaintsService {
	@Autowired
	private ComplaintsDao complaintsDao;
	
	@Autowired
	private CustomerDao dao;
	
	public ResponseStruture<Complaints> saveComplaints(int customer_id,Complaints complaints) {
		Customer customer = dao.getCustomerById(customer_id);
		if (customer == null) {
			throw new NoIdFoundException();
		}
		complaintsDao.saveComplaints(customer_id, complaints);
		ResponseStruture<Complaints> struture = new ResponseStruture<Complaints>();
		struture.setData(complaints);
		struture.setMsg("Complaints is saved");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}
	public ResponseStruture<Complaints> updateComplaints(int customer_id,Complaints complaints) {
		Customer customer = dao.getCustomerById(customer_id);
		if (customer == null) {
			throw new NoIdFoundException();
		}
		complaintsDao.UpdateComplaints(customer_id, complaints);
		ResponseStruture<Complaints> struture = new ResponseStruture<Complaints>();
		struture.setData(complaints);
		struture.setMsg("Complaints is updated");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}
	public ResponseStruture<Complaints> deleteComplaint(int complaint_id) {
		Complaints complaints = complaintsDao.getByIdComplains(complaint_id);
		if (complaints == null) {
			throw new NoIdFoundException();
		}
		complaintsDao.deleteComplaints(complaint_id);
		ResponseStruture<Complaints> struture = new ResponseStruture<Complaints>();
		struture.setData(complaints);
		struture.setMsg("Complaints is deleted");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}
	public List<Complaints> getAllComplaints(){
		List<Complaints> complaints = complaintsDao.getAllComplaints();
		if (complaints == null) {
			throw new NoIdFoundException();
		}
		return complaints;
	}
	public ResponseStruture<Complaints> getComplaintsById(int id) {
		Complaints complaints = complaintsDao.getByIdComplains(id);
		if (complaints == null) {
			throw new NoIdFoundException();
		}
		ResponseStruture<Complaints> struture = new ResponseStruture<Complaints>();
		struture.setData(complaints);
		struture.setMsg("Complaints is Available");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}
	

}
