package com.ty.apnasociety.ApnaSociety_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.apnasociety.ApnaSociety_app.dao.CustomerDao;
import com.ty.apnasociety.ApnaSociety_app.dao.SocietyDao;
import com.ty.apnasociety.ApnaSociety_app.dto.Customer;
import com.ty.apnasociety.ApnaSociety_app.dto.ResponseStruture;
import com.ty.apnasociety.ApnaSociety_app.dto.Society;
import com.ty.apnasociety.ApnaSociety_app.exception.NoIdFoundException;

@Service
public class CustomerService {

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private SocietyDao societyDao;

	public ResponseStruture<Customer> saveCustomer(int societyId, Customer customer) {
		Society society = societyDao.getSocietyById(societyId);
		if (society == null) {
			throw new NoIdFoundException();
		}
		ResponseStruture<Customer> struture = new ResponseStruture<Customer>();
		struture.setData(customer);
		struture.setMsg("Customer is saved");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public List<Customer> getAllCustomers() {
		List<Customer> customer = customerDao.getAllCustomers();
		if (customer == null) {
			throw new NoIdFoundException();
		}
		return customer;
	}

	public ResponseStruture<Customer> getCustomerById(int id) {
		Customer customer = customerDao.getCustomerById(id);
		if (customer == null) {
			throw new NoIdFoundException();
		}
		ResponseStruture<Customer> struture = new ResponseStruture<Customer>();
		struture.setData(customer);
		struture.setMsg("Customer is available");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public ResponseStruture<Customer> updateCustomer(int id, Customer customer) {
		Customer customer2 = customerDao.getCustomerById(id);
		if (customer2 == null) {
			throw new NoIdFoundException();
		}
		ResponseStruture<Customer> struture = new ResponseStruture<Customer>();
		struture.setData(customer);
		struture.setMsg("Customer is updated");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public ResponseStruture<Customer> deleteCustomer(int id) {
		Customer customer = customerDao.getCustomerById(id);
		if (customer == null) {
			throw new NoIdFoundException();
		}
		customerDao.deleteCustomer(id);
		ResponseStruture<Customer> struture = new ResponseStruture<Customer>();
		struture.setData(customer);
		struture.setMsg("Customer is deleted");
		struture.setStatus(HttpStatus.OK.value());
		return struture;

	}
}
