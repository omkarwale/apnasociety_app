package com.ty.apnasociety.ApnaSociety_app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ty.apnasociety.ApnaSociety_app.dao.ChairmanDao;
import com.ty.apnasociety.ApnaSociety_app.dto.Chairman;
import com.ty.apnasociety.ApnaSociety_app.dto.ResponseStruture;
import com.ty.apnasociety.ApnaSociety_app.exception.NoIdFoundException;

@Service
public class ChairmanService {

	@Autowired
	private ChairmanDao chairmanDao;

	public ResponseStruture<Chairman> saveChairman(Chairman chairman) {
		if (chairman == null) {
			throw new NoIdFoundException();
		}
		chairmanDao.saveChairman(chairman);
		ResponseStruture<Chairman> struture = new ResponseStruture<Chairman>();
		struture.setData(chairman);
		struture.setMsg("Chairman is saved");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public ResponseStruture<Chairman> getChairmanById(int id) {
		Chairman chairman = chairmanDao.getChairmanById(id);
		if (chairman == null) {
			throw new NoIdFoundException();
		}
		ResponseStruture<Chairman> struture = new ResponseStruture<Chairman>();
		struture.setData(chairman);
		struture.setMsg("Chairman is Available");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public List<Chairman> getAllChairmans() {
		List<Chairman> chairmans = chairmanDao.getAllChairmans();
		if (chairmans == null) {
			throw new NoIdFoundException();
		}
		return chairmans;
	}

	public ResponseStruture<Chairman> updateChairman(int id, Chairman chairman) {
		Chairman chairman2 = chairmanDao.getChairmanById(id);
		if (chairman2 == null) {
			throw new NoIdFoundException();
		}
		chairmanDao.saveChairman(chairman);
		ResponseStruture<Chairman> struture = new ResponseStruture<Chairman>();
		struture.setData(chairman);
		struture.setMsg("Chairman is updated");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}

	public ResponseStruture<Chairman> deleteChairman(int id) {
		Chairman chairman = chairmanDao.getChairmanById(id);
		if (chairman == null) {
			throw new NoIdFoundException();
		}
		chairmanDao.deleteChairman(id);
		ResponseStruture<Chairman> struture = new ResponseStruture<Chairman>();
		struture.setData(chairman);
		struture.setMsg("Chairman is deleted");
		struture.setStatus(HttpStatus.OK.value());
		return struture;
	}
}
