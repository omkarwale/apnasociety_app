package com.ty.apnasociety.ApnaSociety_app.dto;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;
@Entity
public class Complaints {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String complaint_id;
	private String subject;
	private String status;
	@CreationTimestamp
	private LocalDate dateOfComplent;
	
	@OneToOne
	@JoinColumn
	Customer customer;
	
	public String getComplaint_id() {
		return complaint_id;
	}
	public void setComplaint_id(String complaint_id) {
		this.complaint_id = complaint_id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LocalDate getDateOfComplent() {
		return dateOfComplent;
	}
	public void setDateOfComplent(LocalDate dateOfComplent) {
		this.dateOfComplent = dateOfComplent;
	}
	
}
