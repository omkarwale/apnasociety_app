package com.ty.apnasociety.ApnaSociety_app.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Chairman {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int chairman_id;
	private String chairman_name;
	private String chairman_email;
	private String chairman_password;
	private String chairman_contactNumber;

	public int getChairman_id() {
		return chairman_id;
	}

	public void setChairman_id(int chairman_id) {
		this.chairman_id = chairman_id;
	}

	public String getChairman_name() {
		return chairman_name;
	}

	public void setChairman_name(String chairman_name) {
		this.chairman_name = chairman_name;
	}

	public String getChairman_email() {
		return chairman_email;
	}

	public void setChairman_email(String chairman_email) {
		this.chairman_email = chairman_email;
	}

	public String getChairman_password() {
		return chairman_password;
	}

	public void setChairman_password(String chairman_password) {
		this.chairman_password = chairman_password;
	}

	public String getChairman_contactNumber() {
		return chairman_contactNumber;
	}

	public void setChairman_contactNumber(String chairman_contactNumber) {
		this.chairman_contactNumber = chairman_contactNumber;
	}

}
