package com.ty.apnasociety.ApnaSociety_app.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Society {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int society_id;
	private String society_name;
	private String society_address;
	private int block_number;
	private String society_image;
	private int society_pincode;
	private String city;
	@ManyToOne
	@JoinColumn
	private Chairman chairman;

	public int getSociety_id() {
		return society_id;
	}

	public void setSociety_id(int society_id) {
		this.society_id = society_id;
	}

	public String getSociety_name() {
		return society_name;
	}

	public void setSociety_name(String society_name) {
		this.society_name = society_name;
	}

	public String getSociety_address() {
		return society_address;
	}

	public void setSociety_address(String society_address) {
		this.society_address = society_address;
	}

	public int getBlock_number() {
		return block_number;
	}

	public void setBlock_number(int block_number) {
		this.block_number = block_number;
	}

	public String getSociety_image() {
		return society_image;
	}

	public void setSociety_image(String society_image) {
		this.society_image = society_image;
	}

	public int getSociety_pincode() {
		return society_pincode;
	}

	public void setSociety_pincode(int society_pincode) {
		this.society_pincode = society_pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Chairman getChairman() {
		return chairman;
	}

	public void setChairman(Chairman chairman) {
		this.chairman = chairman;
	}

}
